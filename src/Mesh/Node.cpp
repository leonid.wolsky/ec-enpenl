#include "Node.h"

double Node::distanceTo(Node b){
    return sqrt(pow(this->x-b.x,2.0)+
                pow(this->y-b.y,2.0)+
                pow(this->z-b.z,2.0));
}