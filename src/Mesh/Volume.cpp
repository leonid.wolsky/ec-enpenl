#include "Volume.h"

Volume::Volume(const unsigned int num_faces,
               const double volume,
               const unsigned int index,
               const unsigned int num_states,
                bool isboundary)
{
    if(volume == 0.0) throw std::runtime_error("new_volume: can not create volume with volume 0\n");

    this->nvol = new Volume*[num_faces]();

    this->face = new Face*[num_faces]();

    this->distance = new double[num_faces]();

    this->type = VOLTYPE_INTERNAL;
    this->num_faces = num_faces;
    this->volume = volume;
    this->index = index;
    this->charge = 0;
    this->alpha = 1.0;
    this->Bmod = 0;
    unsigned int i;
    for(i = 0; i < num_faces; i++) this->face[i] = NULL;
    if(isboundary) {
        this->isGhostNode = true;
        this->type = VOLTYPE_BULK;
    }

    this->num_states = num_states;

    this->state = new double[num_states]();

    this->stateIsActive.resize(num_states,true);

    this->bvreaction = NULL;
    this->solution_index = 0;
}

Volume::~Volume(){
    delete[] (this->distance);
    if(this->state) delete[] (this->state);
    delete[] (this->nvol);
    delete[] (this->face);
}