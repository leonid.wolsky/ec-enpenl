#ifndef MESH1D_H
#define MESH1D_H
#include "Mesh.h"

class Mesh1D : public Mesh {
public:
    Mesh1D(std::string filename,unsigned int nv,double mesh_unit = 1e-6);

protected:
    Volume* addVolume(Node *nodes, Face **facelist,
        const unsigned int a, const unsigned int b);
    void setup_boundaries();
};

#endif