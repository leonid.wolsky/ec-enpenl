#ifndef FACE_H
#define FACE_H
#include <cmath>
#include <list>
#include <vector>
#include "Node.h"
#include <stdexcept>
#include <cstring>

typedef enum face_type {FACETYPE_BOUNDARY_LEFT = 0,
                        FACETYPE_BOUNDARY_RIGHT = 1,
                        FACETYPE_BOUNDARY_TOP = 2,
                        FACETYPE_BOUNDARY_BOTTOM = 3,
                        FACETYPE_INTERNAL} face_type_t;

class Volume;

class Face{
public:
    unsigned int num_states;
    face_type_t type = FACETYPE_INTERNAL;
    Node center;
    double area;
    double *flux;
    Volume *nvol[2]; /* Associated volumes */
    int *bcond_type = NULL;        /* Type of boundary condition per state */
    double *boundary_states = NULL; // used for internal boundaries

    Face(const double area, unsigned int num_states);
    void setType(face_type_t type);
    Volume* getOtherVolume(Volume* vol);
    double getField();
    double volumeDistance();
    void setBoundaryState(double* state); // only for the internal boundaries for now
    ~Face();
};

#include "Volume.h"

#endif