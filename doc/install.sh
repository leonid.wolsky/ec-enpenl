#!/bin/sh
wget http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-3.10.5.tar.gz
tar -xzf petsc-3.10.5.tar.gz
rm -f petsc-3.10.5.tar.gz
cd petsc-3.10.5; \
	python2 ./configure --download-mpich; \
	make all test