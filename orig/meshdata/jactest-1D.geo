// mesh element size
lc = 1; // ten micrometers
thickness = 10;
Point(1) = {0.0, 0.0, 0.0, lc};
Point(2) = {thickness, 0.0, 0.0, lc};
Line(1) = {1,2};
