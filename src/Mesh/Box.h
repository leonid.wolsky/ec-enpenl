#ifndef BOX_H
#define BOX_H
#include <cmath>
#include <list>
#include <vector>
#include "Node.h"
#include <stdexcept>
#include "Face.h"

class Box{
public:
    Node a,b; // lower left bottom corner
    Box();
    Box(Node ai, Node bi): a(ai),b(bi){};
    bool contains(Node n);
    void expand(Node n);
    int nodeBoundary(Node *a); // returns the boundary a node lies on
};

#endif