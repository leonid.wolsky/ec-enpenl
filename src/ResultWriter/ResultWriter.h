#ifndef RESULTWRITER_H
#define RESULTWRITER_H

#ifndef PETSC_CLANGUAGE_CXX
#define PETSC_CLANGUAGE_CXX
#endif
#include <petscsys.h>
class Application;
class ResultWriter;
#include "../Application.h"
#include "../Mesh/Mesh.h"
#include "../Mesh/Mesh1D.h"

#include <algorithm>
#include <vector>
#include <stdexcept>
#include <sys/types.h>
#include <sys/stat.h>
#include <fstream> 
#include <iomanip>

class ResultWriter {
private:
    Application* app;
    std::ofstream json;
    int index = 0;
    std::string dir;

    double lastTime = 0;
    double lastE = 0;

    void write_volumes();
    void write_faces();
    void write_vector(std::ofstream& fs, double *vec, size_t len, std::vector<bool>* stateIsActive = NULL);

    int MPIrank();

public:
    bool fullWrite = true;

    ResultWriter(std::string dir, Application* app);
    void close();
    void write_state(double indexval);
};


#endif