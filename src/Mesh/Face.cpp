#include "Face.h"

Face::Face(const double area, unsigned int num_states)
{
    if(area == 0.0) throw std::runtime_error("Face area should not be 0\n");

    this->nvol[0] = NULL;
    this->nvol[1] = NULL;
    this->area = area;
    this->type = FACETYPE_INTERNAL;
    this->flux = NULL;

    this->flux = new double[num_states]();
    this->num_states = num_states;
}

void Face::setType(face_type_t type){
    this->type = type;
    this->bcond_type = new int[this->num_states]();

    if(type == FACETYPE_INTERNAL)
        for(unsigned int i=0;i<this->num_states;i++)
            this->bcond_type[i] = -1;
}

Volume* Face::getOtherVolume(Volume* vol){
    if(this->nvol[0] == vol) return this->nvol[1];
    else return this->nvol[0];
}

double Face::volumeDistance(){
    if(this->nvol[0] == NULL or this->nvol[1] == NULL) return 0;

    Volume* v1 = this->nvol[0];
    Volume* v2 = this->nvol[1];

    double distance = 0;
    if(v1->type != VOLTYPE_BULK && v1->type != VOLTYPE_ELECTRODE)
        distance += v1->center.distanceTo(this->center);
    if(v2->type != VOLTYPE_BULK && v2->type != VOLTYPE_ELECTRODE)
        distance += v2->center.distanceTo(this->center);

    return distance;
}

double Face::getField(){
    if(this->nvol[0] == NULL or this->nvol[1] == NULL) return 0;

    Volume* v1 = this->nvol[0];
    Volume* v2 = this->nvol[1];

    double distance = this->volumeDistance();

    double E = (v1->state[this->num_states-1] - v2->state[this->num_states-1])/distance;
    return (v1->center.x > v2->center.x)? E : -E;
}

Face::~Face(){
    delete[] this->flux;
    if(this->bcond_type) delete[] (this->bcond_type);
    if(this->boundary_states) delete[] (this->boundary_states);
}

void Face::setBoundaryState(double* state){
    if(!this->boundary_states) this->boundary_states = new double[this->num_states]();
    memcpy(this->boundary_states, state, this->num_states * sizeof(double));
}