#ifndef ZONE_H
#define ZONE_H
#include <cmath>
#include <list>
#include <vector>
#include <stdexcept>
#include "Volume.h"
#include "Face.h"
#include "Box.h"
#include <string>
#include <climits>

class BVReaction;

class Zone {

public:
    int type = VOLTYPE_INTERNAL;
    unsigned int index;
    double alpha;
    double charge;
    double *states;
    Box bounds;
    std::list<Volume*> volumes;
    std::string name;
    unsigned int num_states;
    std::vector<std::list<Face*> > boundaries;
    std::vector<unsigned int> activeStates;
    double Bmod;
    BVReaction* bvreaction;
    std::vector<bool> stateIsActive;

    Zone(std::string name, unsigned int index, unsigned int num_states);
    void add_volume(Volume* v);
    void applyToVolumes();
    void updateVolumeProperties();
    void applyActiveStates();
    void applyCharge(double charge);
    void identify_boundaries();
    ~Zone();

    std::list<Face*>& getLeftBoundary();
    std::list<Face*>& getRightBoundary();
};


#endif