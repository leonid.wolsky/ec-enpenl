#include "NPP.h"
#include <cmath>
#include <iostream>
#include <iomanip>
#include <stdlib.h>


void NPP::fnc_time(Volume *v, PetscScalar *in, PetscScalar *din,
        PetscScalar *dst, unsigned int i, Application *ctx)
{
    unsigned int vi = v->solution_index;
    /* Store transient term for i in dst */
    *dst = v->volume * din[vi+i];
}

void NPP::jac_time(Volume *v, PetscScalar *in,
        PetscScalar *din, PetscScalar *dst, unsigned int i, Application *ctx)
{
    /* Calculate dF/dU_t for i and store it at dst */
    *dst = v->volume;
}


void NPP::fnc_spec(Volume *v, PetscScalar *in, PetscScalar *dst,
        unsigned int i, Application *ctx)
{
    unsigned int nos = v->num_states;
    unsigned int vi = v->solution_index;
    ParameterStore *p = ctx->p;

    if(!v->stateIsActive[i]) return;

    PetscScalar c = in[vi+i];
    PetscScalar phi = in[vi+nos-1];

    /* Flux term */
    PetscScalar flux = 0;
    for(unsigned int f = 0; f < v->num_faces; f++) {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];
        Volume* nv = v->nvol[f];
        Face* face = v->face[f];

        PetscScalar c_n, phi_n;
        unsigned int ni = nv->solution_index;

        phi_n = in[ni+nos-1];
        c_n = in[ni+i];

        if(face->bcond_type[nos-1] == 0)
            phi_n = face->boundary_states[nos-1];
        if(face->bcond_type[i] == 0)
            c_n = face->boundary_states[i];

        PetscScalar c_f = 0.5 * (c + c_n);
        PetscScalar alpha = std::max(v->alpha,nv->alpha);

        if(face->bcond_type[i] == 2) // no flux boundary
            flux += 0; 
        else if(!nv->stateIsActive[i])
            flux += 0;
        else
            flux += A/ksi*alpha*p->D[i]*((c_n-c) + p->z[i] * c_f * (phi_n-phi));
    }

    PetscScalar source = 0;

    /* Mass Action Law Reaction */
    for(unsigned int j = 0; j < p->num_reactions; j++) {
        double RcoeffMA = (p->mu[i][j] + p->nu[i][j]) * v->volume;
        double p_f = 1.0, p_b = 1.0;
        for(unsigned int m = 0; m < p->num_species; m++) {
            p_f *= pow(in[vi+m], p->mu[m][j]);
            p_b *= pow(in[vi+m], -p->nu[m][j]);
        }
        source += RcoeffMA * (p->k_f[j] * p_f - p->k_b[j] * p_b);
    }

    *dst = 0 - flux + source;
}

void NPP::jac_spec_local(Volume *v, PetscScalar *in,
        PetscScalar *dst, unsigned int i, Application *ctx)
{
    unsigned int nos = v->num_states;
    unsigned int vi = v->solution_index;
    ParameterStore *p = ctx->p;

    if(!v->stateIsActive[i]) return;

    PetscScalar c = in[vi+i];
    PetscScalar phi = in[vi+nos-1];

    PetscScalar dc = 0;
    PetscScalar dphi = 0;
    unsigned int f;
    for(f = 0; f < v->num_faces; f++) {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];

        Volume* nv = v->nvol[f];
        Face* face = v->face[f];
        unsigned int ni = nv->solution_index;

        PetscScalar c_n, phi_n, c_f;

        phi_n = in[ni+nos-1];
        c_n = in[ni+i];

        if(face->bcond_type[nos-1] == 0)
            phi_n = face->boundary_states[nos-1];
        if(face->bcond_type[i] == 0)
            c_n = face->boundary_states[i];

        c_f = 0.5 * (c + c_n);
        PetscScalar alpha = std::max(v->alpha,nv->alpha);

        PetscScalar factor = A*alpha*p->D[i]/ksi;

        if((face->bcond_type[i] == -1 || face->bcond_type[i] == 0) && nv->stateIsActive[i]){
            dc += factor * (1 - p->z[i] * 0.5 * (phi_n-phi));
            dphi += factor * p->z[i] * c_f;
        }
    }
    dst[i] = dc;
    dst[nos-1] = dphi;

    /* Mass Action Reaction */
    for(unsigned int k = 0; k < p->num_species; k++) {
        double ds = 0;
        for(unsigned int j = 0; j < p->num_reactions; j++) {
            double RcoeffMA = (p->mu[i][j] + p->nu[i][j]) * v->volume;
            PetscScalar p_f = 1.0, p_b = 1.0;

            for(unsigned int m = 0; m < p->num_species; m++) {
                if(k == m) {
                    if(p->mu[m][j] > 1) p_f *= pow(in[vi+m], p->mu[m][j] -1);
                    if(-p->nu[m][j] > 1) p_b *= pow(in[vi+m], -p->nu[m][j] -1);
                } else {
                    p_f *= pow(in[vi+m], p->mu[m][j]);
                    p_b *= pow(in[vi+m], -p->nu[m][j]);
                }
            }
            ds += RcoeffMA * (p->k_f[j] * p->mu[k][j] * p_f - p->k_b[j] * (-p->nu[k][j]) * p_b);

        }
        dst[k] += ds;
    }

}

void NPP::jac_spec_neighbour(Volume *v, PetscScalar *in,
        PetscScalar *dst, unsigned int i, unsigned int f, Application *ctx)
{
    unsigned int nos = v->num_states;
    unsigned int vi = v->solution_index;
    Volume *nv = v->nvol[f];
    ParameterStore *p = ctx->p;
    Face* face = v->face[f];

    if(!v->stateIsActive[i]) return;
    if(!nv->stateIsActive[i]) return;

    if(face->bcond_type[i] == -1){

        PetscScalar dphi = 0,dc = 0;
        unsigned int ni = nv->solution_index;

        PetscScalar c, phi, c_n, phi_n, c_f;
        phi = in[vi+nos-1];
        phi_n = in[ni+nos-1];

        c = in[vi+i];
        c_n = in[ni+i];
        c_f  = 0.5 * (c + c_n);
       
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];
        PetscScalar alpha = std::max(v->alpha,nv->alpha);

        PetscScalar factor = A*alpha*p->D[i]/ksi;

        dc = -factor * (1 + p->z[i] * 0.5 * (phi_n-phi));
        dst[i] = dc;

        dphi = -factor * p->z[i] * c_f;
        dst[nos-1] = dphi;
    }
}



void NPP::fnc_pois(Volume *v, PetscScalar *in, PetscScalar *dst,
        Application *ctx)
{
    unsigned int vi = v->solution_index;
    unsigned int nos = v->num_states;
    ParameterStore *p = ctx->p;

    PetscScalar phi = in[vi+nos-1];

    /* Source term */
    PetscScalar rho = v->charge, source;
    for(unsigned int i = 0; i < nos-1; i++) {
        if(v->stateIsActive[i]) rho += in[vi+i] * p->z[i];
    }

    source = v->volume * rho * p->debye;

    /* Flux term */
    PetscScalar flux = 0;
    for(unsigned int f = 0; f < v->num_faces; f++) {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];

        Volume* nv = v->nvol[f];
        Face* face = v->face[f];
        PetscScalar phi_n;
        if(face->bcond_type[nos-1] == 0){
            phi_n = face->boundary_states[nos-1];
        } else {
            unsigned int ni = nv->solution_index;
            phi_n = in[ni+nos-1];
        }

        if(face->bcond_type[nos-1] == 2)
            flux += 0;
        else 
            flux += A / ksi * (phi_n - phi);
    }
    *dst = flux + source;
}


void NPP::jac_pois_local(Volume *v, PetscScalar *in,
        PetscScalar *dst, Application *ctx)
{
    ParameterStore *p = ctx->p;
    unsigned int nos = v->num_states;

    PetscScalar dphi = 0;

    for(unsigned int f = 0; f < v->num_faces; f++) {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];

        Face* face = v->face[f];
        if(face->bcond_type[nos-1] == 2)
            dphi += 0;
        else
            dphi += -A/ksi;
    }
    dst[nos-1] = dphi;

    for(unsigned int i = 0; i < nos-1; i++) {
        if(v->stateIsActive[i]) dst[i] = v->volume * p->debye * p->z[i];
    }
}

void NPP::jac_pois_neighbour(Volume *v, PetscScalar *in,
        PetscScalar *dst, unsigned int f, Application *ctx)
{
    Face* face = v->face[f];
    Volume* nv = v->nvol[f];
    unsigned int nos = nv->num_states;

    if(face->bcond_type[nos-1] == 2)
        dst[nos-1] += 0;
    else if(face->bcond_type[nos-1] == 0)
        dst[nos-1] += 0;
    else
        dst[nos-1] = v->face[f]->area/v->distance[f];
}


void NPP::fnc_flux(Face *face, Application *ctx)
{
    Volume *v1, *v2;
    v1 = face->nvol[0];
    v2 = face->nvol[1];
    if(!v1||!v2) throw std::runtime_error("A face is not properly connected");

    ParameterStore *p = ctx->p;
    unsigned int nos = v1->num_states;

    // PetscScalar ksi = v2->center.x - v1->center.x;
    PetscScalar ksi = face->volumeDistance();
    ksi = (v1->center.x > v2->center.x) ? -1*ksi : ksi;
    PetscScalar dphi, dc, c_f;
    dphi = (v2->state[nos-1] - v1->state[nos-1]);

    face->flux[nos-2] = 0;
    face->flux[nos-1] = 0;

    PetscScalar alpha = std::max(v1->alpha,v2->alpha);

    for(unsigned int i = 0; i < nos-1; i++) {
        dc = v2->state[i] - v1->state[i];
        c_f = 0.5*(v2->state[i] + v1->state[i]);

        if(!(v1->stateIsActive[i] && v2->stateIsActive[i]))
            face->flux[i] = 0;
        else if(face->bcond_type[i] > 1)
            face->flux[i] = 0;
        else
            face->flux[i] = -alpha*p->D[i]/ksi*(dc + p->z[i]*c_f*dphi);
    }
}