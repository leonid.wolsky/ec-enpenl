# Model assumptions

## Nafion (N117)

```math
\text{charge density} = \frac{1 mole\text{ sulfonic acid}}{1.1kg\text{ Nafion(dry)}}
```
From: https://en.wikipedia.org/wiki/Nafion

```math
360g/m^2 @ 183\mu m\text{ thickness (dry)}
```
From: https://nafionstore-us.americommerce.com/Shared/Bulletins/N115-N117-N1110-Product-Bulletin-Chemours.pdf

Roughly 2M dry. 1M swollen is realistic.

Diffusion coefficient for Protons 0.5e-6 cm2/s = 0.5e-10 m2/s

From: dx.doi.org/10.1021/la401453e

## Pedot 

Charge density: Bonfil assumes 2e-12 to 2e-2 mol/L as his simulation range

10.1038/srep18805 propose 1e20cm-3 as charge carrier density.
roughly 0.2M

Carrier mobility and diffusion coefficient are related by Einstein's relation: https://en.wikipedia.org/wiki/Einstein_relation_(kinetic_theory)l

Bonfil schlägt 1e-5 bis 0.5 m2/Vs carrier mobility vor.
Entspricht D_h+ = 2.5e-7 m2/s bis 0.0118 m2/s

https://tel.archives-ouvertes.fr/tel-00968227/document
schlagen auf der anderen Seite Mobilitäten 1.4e-3 cm2/Vs vor :thinking:


https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6050616
Messen 0.77 cm2/Vs 
Entspricht Bonfil's Annahme


## Notes
Fluxes should be in mol/s/m2
Leitfähigkeit: j = sigma *E
beispiel für resistor: 100A/m2/(0.1V/1e-2m) = 10S/m = 0.1 S/cm
Sigma Aldrich und Heraeus sagen 1-1000 S/cm

Vergleich: 500Ohm bei 1um*5mm/10mm entspräche 4000 S/m = 40 S/cm

Bei sigma = 40 S/cm und D_pedot = 2.5e-7 m2/s => c_pedot = 4 mol/L

Missing:
h+ in pedot?