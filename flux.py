#!/usr/bin/env python3
import sys
from py.Npp1DPlots import *

simulation = sys.argv[1] if len(sys.argv) > 1 else "tybrandt_cv"

d = Npp1DPlots('outdata/' + simulation)

d.showFlux()

plt.show()