// mesh element size
lc = 1; // ten micrometer
thickness = 100; // boundary layer thickness [10mu]
memb_thickness = 10; // membrane thickness [10mu]
Point(1) = {0.0, 0.0, 0.0, lc/10};
Point(2) = {thickness, 0.0, 0.0, lc/10};
Point(3) = {thickness+memb_thickness, 0.0, 0.0, lc/10};
Point(4) = {thickness+memb_thickness+thickness, 0.0, 0.0, lc/10};
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};

Field[1] = BoundaryLayer;
Field[1].NodesList = {2, 3};
Field[1].hfar = lc;
Field[1].hwall_n = lc/100000;
Field[1].hwall_t = lc/100000;
Field[1].ratio = 1.01;

Background Field = 1;
