# Normalization

epsilon = sqrt(e_r*e0*RT/(F^2L0^2c0))

Für zb. e_r = 81
sqrt(81*8.85e-12*8.3144*293/(1e-12*1000*96485^2))
0.00043

# Conductivity of electrolytes
0.1M NaCl has 10mS/cm, that is 1e5 Ohmmeter
for 300um length: 1e5 * 300e-6 ohm m^2 = 30 Ohm m^2

# Todo:

sum flux for all species crossing boundary and fix flux calculation
test electrodialysis with no membrane for correct conductivity

# Todo for Dirk:
- Read a lot of code