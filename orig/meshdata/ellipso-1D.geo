// mesh element size
lc = 1; // one micrometer
bulk = 1000;
memb_thickness = 0.65; // membrane thickness [1mu]
Point(1) = {0.0, 0.0, 0.0, lc/10000};
Point(2) = {memb_thickness, 0.0, 0.0, lc/10000};
Point(3) = {bulk, 0.0, 0.0, lc};
Line(1) = {1,2};
Line(2) = {2,3};

Field[1] = BoundaryLayer;
Field[1].NodesList = {1, 2};
Field[1].hfar = lc;
Field[1].hwall_n = lc/10000;
Field[1].hwall_t = lc/10000;
Field[1].ratio = 1.01;

Background Field = 1;
