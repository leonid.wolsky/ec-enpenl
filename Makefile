### VARIABLES

PETSC_DIR=petsc-3.10.5/arch-linux2-c-debug

ifeq ($(wildcard petsc-3.10.5),) 
	ifdef R_SLURM_ROLES
		PETSC_DIR=/usr/local_rwth/sw/petsc/3.10.4/intel_19.0.1.144-intelmpi_2018.4.274
	endif
endif

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	PETSC_DIR=/opt/homebrew/Cellar/petsc/3.15.4
endif

include $(PETSC_DIR)/lib/petsc/conf/petscvariables
CFLAGS=-Werror -ggdb -Wall -D_GNU_SOURCE -Wno-nullability-completeness
INCLUDES=$(PETSC_CC_INCLUDES)

LDFLAGS=-L$(LD_LIBRARY_PATH) -L$(PETSC_DIR)/lib -lm $(PETSC_LIB_BASIC)
ifeq ($(UNAME_S),Darwin)
	LDFLAGS=-L$(PETSC_DIR)/lib -lm $(PETSC_LIB_BASIC)
endif

GMSH = gmsh

export LD_LIBRARY_PATH := $(LD_LIBRARY_PATH):$(PETSC_DIR)/lib

### RUNTIME OPTIONS
MPIOPTS=-np 1 # currently no parallel support

SRC := $(wildcard src/*.cpp) $(wildcard src/**/*.cpp) 
OBJ := $(SRC:%.cpp=obj/%.o)


# Meshes
GEO := $(wildcard meshdata/*-1D.geo)
MSH := $(GEO:%.geo=%.msh)


# Systems
SYS := $(wildcard sysdata/*.sys)
DEF := $(SYS:%.sys=%.def)

# PetSc Options
TOL=-snes_atol 1e-8 -snes_rtol 1e-30 -snes_stol 1e-16 -ts_pseudo_fatol 1e-8 -ts_pseudo_frtol 1e-30
MONITOR=-snes_converged_reason #-snes_monitor 
OPTS=$(MONITOR) $(TOL) -snes_max_it 10 # -info

.PHONY: clean all debug valgrind test sys
all: memristor sys

run: sys memristor
	@rm -rf outdata/memristor2
	@mv outdata/memristor outdata/memristor2 || true # cache last result
	@echo "[EX] >>> "$<
	$(LDPATH) $(MPIEXEC) -n 1 ./memristor $(OPTS) 

run-parallel: sys memristor
	$(LDPATH) $(MPIEXEC) -n 2 ./memristor $(OPTS) 

### PATTERNS
obj/%.o: %.cpp
	@echo "[CC] >>> "$<
	@mkdir -p $(dir $@)
	@$(CXX) $(CFLAGS) $(INCLUDES) -std=c++11 -c $< -o $@ 
%1D.msh: %1D.geo
	@echo "[GM] >>> "$<
	$(GMSH) -1 -format msh22 $< -o $@ 
	### Gmsh -1 -format msh22 input.geo -o output.msh

%.def: %.sys
	@echo "[DG] >>> "$<
	@sysdata/defgen3.py $< > $@

### MESHES
geo: $(MSH)
### SYSTEMS
sys: $(DEF)

### APPLICATIONS

bpm: $(OBJ) obj/bpm.o
	@echo "[LD] >>> "$@
	$(CXX) $(OBJ) obj/bpm.o -o $@  $(LDFLAGS)

bpm-run: sys bpm
	@echo "[EX] >>> "$<
	$(LDPATH) $(MPIEXEC) -n 1 ./bpm $(OPTS) 


clean:
	rm -rf obj/*