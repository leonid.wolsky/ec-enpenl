#ifndef PETSC_CLANGUAGE_CXX
#define PETSC_CLANGUAGE_CXX
#endif
#include <petscsys.h>

#include "src/ParameterStore.h"
#include "src/Application.h"
#include "src/Models/NPP.h"
#include "src/Mesh/Mesh1D.h"
#include "src/ResultWriter/ResultWriter.h"
#include "src/ResultWriter/ResultLoader.h"
#include <string>
#include <map>
#include "math.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <list>

int main(int argc, char **argv)
{
    PetscErrorCode ierr;
    ierr = PetscInitialize(&argc, &argv, NULL, NULL);
    CHKERRXX(ierr);

    /* Set up simulation */
    // ParameterStore p("sysdata/bpm_wor.def");
    ParameterStore p("sysdata/bpm.def");

    auto nm = p.nameMap;

    NPP model;

    Application app(&p, (Model *)&model);
    unsigned int n = app.nv;

    double h_p = 9.77e-13, oh_m = 1.02e-2, k_p = 0.0, hco3_m = 1.02e-2, co3_2m = 4.9e-1;
    k_p = -h_p + oh_m + hco3_m + 2*co3_2m;

    double initialDEF[] = {h_p, oh_m, k_p, co3_2m, hco3_m, 2.24e-8, 0}; // [mol/l] see bpm.sys ; DEF = DEFAULT

    double k_p_ael = -1.01e-14 + 0.99 + 2*0.01 + 2.16e-6;
    double initialAEL[] = {1.01e-14, 0.99, k_p_ael, 0.01, 2.16e-6, 4.88e-14, 0}; // [mol/l] see bpm.sys

    double k_p_cel = -2.83e-9 + 3.54e-6 + 2*1.62e-2 + 9.78e-1;
    double initialCEL[] = {2.83e-8, 3.54e-6, k_p_cel, 1.62e-2, 9.78e-1, 6.18e-3, 0}; // [mol/l] see bpm.sys
    // {H+, OH-, K+,CO3-2, HCO3-, CO2, Potential}

    Mesh1D mesh("meshdata/bpm-1D.msh", n);
    mesh.set_state(&initialDEF[0], n);

    Zone *left_zone = mesh.getZone("LEFT");
    left_zone->applyToVolumes();

    Zone *aem_zone = mesh.getZone("AEM");
    aem_zone->states[1] = 1 + oh_m;
    aem_zone->charge = 1;
    aem_zone->applyToVolumes();

    Zone *wdl_zone = mesh.getZone("WDL");
    wdl_zone->applyToVolumes();

    Zone *cem_zone = mesh.getZone("CEM");
    cem_zone->states[2] = 1 + k_p;
    cem_zone->charge = -1;
    cem_zone->applyToVolumes();

    Zone *right_zone = mesh.getZone("RIGHT");
    right_zone->applyToVolumes();

    mesh.initialize();
    app.setMesh((Mesh *)&mesh);

    for (auto& f : left_zone->getLeftBoundary()) {
        f->setBoundaryState(&initialDEF[0]);
    }
    for (auto& f : right_zone->getRightBoundary()) {
        f->setBoundaryState(&initialDEF[0]);
    }

    int bcond[] = {0, 0, 0, 0, 0, 0, 0, 0}; // see bpm.sys
    mesh.setBoundaryCondition(left_zone->getLeftBoundary(), &bcond[0]);
    mesh.setBoundaryCondition(right_zone->getRightBoundary(), &bcond[0]);

    ResultWriter rw("outdata/bpm_init", &app);
    app.rw = &rw;
    app.pseudoTransient(5e5);
    app.transient(1e1,1e-1);

    for (auto& f : left_zone->getLeftBoundary()) {
        f->setBoundaryState(&initialAEL[0]);
    }
    for (auto& f : right_zone->getRightBoundary()) {
        f->setBoundaryState(&initialCEL[0]);
    }

    mesh.setBoundaryState(left_zone->getLeftBoundary(), 0, n-1); // 4 are thermal volt  1 V = 40 thermal volts

    app.pseudoTransient(5e5);
    rw.close();

    /* Finish and exit */
    app.destroy();
    return PetscFinalize();
}
