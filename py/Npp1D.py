#!/usr/bin/env python3
import numpy.fft as fft
import scipy.constants
import numpy as np
import json
import pandas as pd


class Npp1D:

    T = 298.15
    anim = []
    states = []
    fluxes = []
    charges = []
    times = []
    simulationComplete = True

    def __init__(self, dir):
        self.dir = dir

        self.loadIndex()

        self.volumes = np.loadtxt(dir + "/volumes", delimiter=",")
        self.faces = np.loadtxt(dir + "/faces", delimiter=",")

    def refresh(self):
        if not self.simulationComplete:
            self.loadIndex()

    def loadIndex(self):

        ij = open(self.dir + "/index.json", "r")
        try:
            self.index = json.load(ij)
        except:
            try:
                ij.seek(0)
                self.index = json.loads(ij.read() + '}}')
                self.simulationComplete = False
            except:
                return

        self.nv = self.index["nv"]
        self.names = self.index["species"]
        self.count = len(self.index["indices"])
        self.charges = [float(c) for c in self.index["charges"]]
        self.zones = self.index["zones"]

        self.states = [0]*self.count
        self.fluxes = [0]*self.count

        self.times = np.array([x['time'] for x in self.index['indices'].values()])

        self.timeEvolution = {
            'time': np.array([x['time'] for x in self.index['indices'].values()]),
            'fluxes': np.array([x['fluxes'] for x in self.index['indices'].values()]),
            'voltage': np.array([x['voltage'] for x in self.index['indices'].values()])
        }

    def getstate(self, index):
        if(not isinstance(self.states[index], (np.ndarray))):
            self.states[index] = np.genfromtxt(
                self.dir + "/state/" + str(index), delimiter=",")

        return self.states[index]

    def getflux(self, index):
        if(not isinstance(self.fluxes[index], (np.ndarray))):
            self.fluxes[index] = np.genfromtxt(
                self.dir + "/flux/" + str(index), delimiter=",")

        return self.fluxes[index]

    def getIV(self):
        # t = np.array(self.times)
        # pt = self.timeEvolution['ptotal']
        # v = self.timeEvolution['voltage'] / 40
        # dpdt = np.diff(pt) / np.diff(t)
        # t2 = (t[:-1] + t[1:]) / 2
        # v2 = (v[:-1] + v[1:]) / 2
        # i = dpdt * 1e-3 * scipy.constants.Avogadro * scipy.constants.elementary_charge
        # return t2, i, v2
        pass

    def impedance(self):
        
        def fftransform(signal):
            nfft = signal.size
            n = np.floor(0.5 * nfft)
            n = int(n)
            s_ft = fft.fft(signal)
            idx = np.argmax(np.abs(s_ft[1:n]))
            z = s_ft[idx + 1] / n

            return z

        t, i, v = self.getIV()

        V0 = fftransform(v)

        I = fftransform(i)

        imp = V0/I

        return imp

    
    def exportIV(self, file='plots/output.csv'):
        t, i, v = self.getIV()
        df = pd.DataFrame({'Time': t, 'Potential (V)': v, 'Flux (A/m2)': i})
        df.to_csv(file)
