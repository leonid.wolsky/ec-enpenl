// mesh element size
lc = 1; // one micrometer
lc_fine_internal = lc/1000000;
lc_boundary = lc/100;
h = 2; // length of refinement
thickness = 100; // boundary layer thickness [1mu]
memb_thickness = 100.0; // membrane thickness [1mu]
Point(1) = {0.0, 0.0, 0.0, lc_boundary};
Point(2) = {h,0,0,lc};
Point(3) = {thickness-h, 0.0, 0.0, lc};
Point(4) = {thickness, 0.0, 0.0, lc_fine_internal};
Point(5) = {thickness+h, 0.0, 0.0, lc};
Point(6) = {thickness+memb_thickness-h, 0.0, 0.0, lc};
Point(7) = {thickness+memb_thickness, 0.0, 0.0, lc_fine_internal};
Point(8) = {thickness+memb_thickness+h, 0.0, 0.0, lc};
Point(9) = {thickness+memb_thickness+thickness-h, 0.0, 0.0, lc};
Point(10) = {thickness+memb_thickness+thickness, 0.0, 0.0, lc_boundary};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,9};
Line(9) = {9,10};

Physical Line ("LEFT") = {1,2,3};
Physical Line ("MEMBRANE") = {4,5,6};
Physical Line ("RIGHT") = {7,8,9};