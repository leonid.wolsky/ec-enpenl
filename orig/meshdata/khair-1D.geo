// mesh element size
lc = 1; // one micrometer
thickness = 100; // boundary layer thickness [1mu]
Point(1) = {0.0, 0.0, 0.0, lc/10};
Point(2) = {thickness, 0.0, 0.0, lc/10};
Line(1) = {1,2};

Field[1] = BoundaryLayer;
Field[1].NodesList = {1, 2};
Field[1].hfar = lc;
Field[1].hwall_n = lc/10000.0;
Field[1].hwall_t = lc/10000.0;
Field[1].ratio = 1.01;

Background Field = 1;
