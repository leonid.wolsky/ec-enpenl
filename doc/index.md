# EnPEn introduction 

## C++ general things
- Pointer vs Reference vs Variable
- C style arrays
- Objektorientierung


### Merkwürdige Syntax
```
(const char*)"sysdata/nvrt_simplified.def"
```
Konvertiert einen String in das C übliche Format, das einen Pointer auf einen Array of Char (Buchstabe) übergibt. 

```
ParameterStore p((const char*)"sysdata/nvrt_simplified.def");
```
Erzeugt neuen Parameterstore mit Name p und übergibt Pfad an den Parameterstore Constructor. 

### Pointer vs Reference vs Variable
```
Point punkt(x,y)
```
Erzeugt ein Objekt punkt vom Typ point.

```
Point* p = new Point(x,y)
```
Datentyp Pointer of Point. Enthält die Adresse, an der ein Point Objekt gespeichert ist. Mit ```->``` oder ```(*name).``` kann auf die Eigenschaften zugegriffen werden. Das heißt ```p->x``` entspricht ```(*p).x```.

```
Point* p = &punkt
```
Erzeugt einen Pointer auf die existiernde Variable punkt. 


```
Point& punkt_referenz = punkt;
```
Referenzen funktionieren ähnlich wie Pointer. Können im aktuellen Kontext wie normale Variablen verwendet werden. Das heißt Zuweisen mit = und lesen mit Variablennamen. 
Eigentlich sollten wir nur noch Referenzen verwenden, wo möglich. Da cooler als Pointer :)

### C style arrays 
(unpraktisch und veraltet, tauchen aber noch im code auf)
```
int example[] = {1,2,3};
```
Zugriff mit example[1] zum Beispiel für das zweite Element. Größe ist fest. Zugriff auf Elemente außerhalb des Arrays (bsp. ab Index 3) greift auf fremenden Speicher zu. Muss also vermieden werden. 
Intern sind c arrays Pointer. Die Klammern [] derefferenzieren den Pointer und zählen ihn um die Zahl in den Klammern hoch. 


### Type casts
```
(Model*) &model
```
Type cast vom Pointer Typ child class auf parent class. Hier damit verschiedene Modelle im gleichen Kontext verwendet werden können. (Stichwort Polymorphismus)

### Objektorientierung
Ermöglicht logische Datenstrukturierung und Zugriffskontrolle. Dabei wird eine Datenstruktur mit Methoden(Funktion) verknüpft, die verwenden/bearbeiten. Ermöglicht Wiederverwendbarkeit von Code.

Siehe: https://de.wikibooks.org/wiki/C%2B%2B-Programmierung/_Objektorientierte_Programmierung


## How does a solver work?
Grundlage: 
- Newton Verfahren https://www.youtube.com/watch?v=jB0eUwO0wm4
Findet die Lösung einer Gleichung f(x) = 0, indem mit Hilfe der Ableitung iteriert wird:
x_n+1 = x_n - f(x_n)/f'(x_n)
In EnPEn sind x Konzentrationen, Potential oder Zeitableitung von Konzentrationen auf den verschiedenen Volumen.

- Euler Verfahren https://en.wikipedia.org/wiki/Euler_method 
Integriert eine Gewöhnliche Differenzialgleichung f'(t) durch
f(t+dt) = f(t) + dt f'(t)

- Implizites Eulerverfahren http://web.mit.edu/10.001/Web/Course_Notes/Differential_Equations_Notes/node3.html
Vermeidet Oszillationen durch Iterative Lösung von 
f(t+dt) = f(t) + dt f'(t+dt)
Wird in EnPEn verwendet.


## Data structures
- Mesh !!
- Volume
- Face
- Model 
- Application

![](img/enpen_structure.jpg)

## App
Datenstruktur verwaltet gesamte Simulation, damit von überall auf alles konsistenz zugegriffen werden kann.
Enthält aber auch die Methoden um die Simulation zu starten (transient, pseudoTransient, ...).

## ParameterStore
Enthält Diffusionskoeffizienten, Reaktionskonstanten und so weiter. Quasi alle Konstanten für die Simulation

## Mesh
Hat: 
- Liste von Volumes (Pointer)
- Liste von Faces (Poiner)
- Liste mit Faces/Boundaries
- Liste mit den verschiedenen Zonen

Mesh Klasse lädt die Volumes, Faces, etc. und verknüpft sie. Andere Klassen können die verknüpfte Datenstruktur direkt verwenden. 
Zum Beispiel indem über alle Volumes iteriert wird. 

### Volumes
Volumen haben bestimmte Eigenschaften wie Position, Permittivität (konstant), Zustände wie Konzentration (variabel) an der Stelle (zb.) usw., aber auch **Verknüpfungen mit den Nachbarvolumen und Nachbar Faces**.

Solver ließt die Zustände aus dem Mesh, stellt den Lösungsvektor auf und schreibt die Lösungen nach jedem Simulationsschritt wieder ins Mesh.

### Faces
Genau so kennen die Faces ihre Nachbarvolumen. So kann durch das ganze Mesh iteriert werden.

### Zones
Eine Zone enthält einfach mehrere Volumen um einfacher drauf zugreifen zu können.

### Model
Enthält Funktionen, die die Nernst Planck Poisson Gleichungen im Finite Volumen System repräsentieren und aus denen der Lösungsvektor aufgestellt wird.

### Solver
Verwendet das Modell um Lösungsvektoren für PETSc aufzustellen. 



## Verwendung (Code)


```c++
int main(int argc, char **argv)
{
    PetscErrorCode ierr;
    ierr = PetscInitialize(&argc, &argv, NULL, NULL); CHKERRXX(ierr);

    // Konstanten werden aus Datei in p gelesen.
    ParameterStore p((const char*)"sysdata/nvrt_simplified.def");

    // Modell ausgewählt und initialisiert.
    NPPModel model;

    // Application wird erstellt und mit p und model verknüpft.
    Application app(&p, (Model*) &model);

    // Anzahl der Simulationszustände
    unsigned int n = app.nv;

    // Mesh wird aus .msh Datei geladen, für n Zustände initialisiert, auf 1e-6 Meter eingestellt. Die Box gibt die Größe der Mesh Geometrie an.
    Mesh1D mesh((char*)"meshdata/nvrt-1D.msh",n,1e-6,Box(Node(0, -1, -1),Node(208.0, -1, -1)));
    

    // Anfangskonzentrationen in der gleichen Reihenfolge wie die Stoffe in der .def Datei auftauchen.
    double initial[] = {0.4e1,7.4e-3,0}; // Values normalized to M

    // Setzt Anganskonzentrationen auf gesamtem Mesh
    mesh.set_state(&initial[0], n);

    // Und passt sie in einzelnen Bereichen wieder an.
    Zone& pedot_l = mesh.getZone("PEDOTL");
    // Nicht alle Zustände werden für alle Bereiche simuliert. Hier 0,1,2 = alle.
    pedot_l.activeStates.assign({0,1,2});
    // Hintergrundladung für diesen Bereich setzen. 
    pedot_l.charge = -0.4e1 - 7.4e-2;
    // und anwenden.
    pedot_l.applyToVolumes();
    Zone& pedot_r = mesh.getZone("PEDOTR");
    pedot_r.activeStates.assign({0,1,2});
    pedot_r.charge = -0.4e1 - 7.4e-3;
    pedot_r.applyToVolumes();

    Zone& naf = mesh.getZone("Nafion");
    naf.type = VOLTYPE_MEMBRANE;
    naf.activeStates.assign({1,2});
    naf.states[1] = 0.69e-1;
    naf.charge = -0.69e-1;
    naf.alpha = 1e-2;
    naf.applyToVolumes();

    // Konfiguriert Mesh Datenstrukturen
    mesh.initialize();

    // Mesh mit App verknüpfen
    app.setMesh((Mesh*) &mesh);


    // Konzentrationen am Rand werden gesetzt, falls die 0 Boundary condition verwendet wird. Siehe unten 
    mesh.set_boundary_state(FACETYPE_BOUNDARY_RIGHT, &initial[0], n);
    mesh.set_boundary_state(FACETYPE_BOUNDARY_LEFT, &initial[0], n);

    // Initialisiert die Ausgabe in outdata/nvrt.dirk
    ResultWriter rw("outdata/nvrt.dirk", &app);

    // Für die Initialisierung werden durchlässige Boundaries verwendet.
    int bcond_type[] = {0,0,0};

    // Und auf die Ränder Links und Rechts der Zones angewandt.
    mesh.setBoundaryCondition(pedot_l.getLeftBoundary(),&bcond_type[0]);
    mesh.setBoundaryCondition(pedot_r.getRightBoundary(),&bcond_type[0]);

    // Der aktuelle Zustand wird ausgegeben (in die Ausgabedatei).
    rw.write_state(0);

    // Lässt das aktuelle Modell in den steady state laufen.
    app.pseudoTransient();
    rw.write_state(0);

    int bcond_closed[] = {4,2,0};
    mesh.setBoundaryCondition(pedot_l.getLeftBoundary(),&bcond_closed[0]);
    mesh.setBoundaryCondition(pedot_r.getRightBoundary(),&bcond_closed[0]);

    app.pseudoTransient();
    rw.write_state(0);

    // Verändert das Potential am Rand auf 2 thermische Volt = 50mV
    // Argumente: Boundary, Value (Volt hier), index des Zustandes. Hier der letzte für das Potential. 
    mesh.modify_boundary_state(FACETYPE_BOUNDARY_RIGHT, 2, n-1);

    // Verknüpft den ResultWriter mit app. Jetzt wird jeder Zeitschritt gespeichert.
    app.rw = rw;
    // Simuliert in 1e-3s Schritten für eine Sekunde die Zeitliche Entwicklung
    app.transient(1,1e-3);

    // Schließt die Ausgabedateien
    rw.close();

    /* Finish and exit */
    // Räumt PETSc Objekte auf, bevor PETSc abgeschlossen wird.
    app.destroy();
    return PetscFinalize();
}
```

### Boundary conditions
Example:

```
int bcond_opencircuit[] = {2,2,2};
mesh.setBoundaryCondition(pedot_l.getLeftBoundary(),&bcond_opencircuit[0]);
```
Position indicates the state to which the boundary applies. In this case: substance1, substance2 and potential.
```&bcond_opencircuit[0]``` passes a pointer to the start of the array. 

For substances: 
- 0 -> fixed concentration
- 2 -> closed boundary
- 4 -> electroneutral

For the potential:
- 0 -> fixed potential
- 2 -> floating potential


## How to run EnPEn

### Installation

(im RWTH Cluster schon installiert)

Required packages: libblas-dev liblapack-dev mpich valgrind libcr-dev libhwloc-dev livevent-dev
Python2: plex

Im Cluster Gmsh manuell runterladen und Version 4.3 in den Home Ordner entpacken.

PETSc
```
./install.sh
``` 

### Make
Am Beispiel nvrt:
``` 
make nvrt-run
```

Außerdem: 
```
./runImpedance.py nvrt
```
Lässt eine Impdanzanalyse mit mehreren parallelen Prozessen laufen.

### Plot
Ausgabedateien werden in ./outdata erzeugt.

Verschiedene Darstellungsoptionen:
- ./plot.py nvrt zeigt alle Zeitschritte in einem Diagramm mit verschiedenen Farben an.
- ./animate.py nvrt animiert die Zeitschritte nacheinander.
- ./flux.py nvrt zeigt nur den Strom und die Spannung über der Zeit an.
- ./GUI.py startet GUI welche den Flux und die Konzentration über alle Zeitschritte hinweg darstellt. Verschiede species können betrachtet werden und zoom ist mgl.

Außerdem:
- ./cv.py nvrt zeigt Strom über Spannung an.
- ./impedance.py nvrt_eis ließt eine Sammlung verschiedener Simulationen mit dem Namen nvrt_eis_frequency ein, berechent die Impdanzen und zeichnet Nyquist und Bode plots.


## Supporting software

### Make
Automatisiert den Build Prozess (Compile, etc.). Befehle werden in einem Makefile definiert, sehen etwa so aus: 
```
nvrt: $(OBJ) obj/nvrt.o
	@echo "[LD] >>> "$@
	$(CXX) $(OBJ) obj/nvrt.o -o $@  $(LDFLAGS)
``` 
Bedeutet: Erzeuge alle $(OBJ) obj/nvrt.o und führe die Kommandos darunter aus.
$(CXX) ist eine Variable, in der der c++ compiler steht. Bsp gcc. Ansonsten einfach ein Terminal Befehl, der ausgefürht wird.

Aufgerufen über:
```
make nvrt
```


### Gmsh
Gmsh verwaltet Mesh und Zone names: http://gmsh.info/doc/texinfo/gmsh.html#Geometry-commands
Wird automatisch von make aufgerufen oder durch ```make geo```.


Elemente sind Punkte mit 3 Koordinaten und einer Meshauflösung lc
```
Point(1) = {0.0, 0.0, 0.0, lc_fine};
```

Linien, die zwei Punkte verbinden 
```
Line(1) = {1,2};
``` 

Und benannte Sammlungen von Linien = Zonen, auf die aus EnPEn zugegriffen wird. Hier 3 Linien
```
Physical Line ("PEDOTL") = {1,2,3};
``` 

Auf Basis dieser Geometrie wird ein msh File generiert.

### PETSc
PETSc Dokumentation https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/TS/index.html 

### git
Versionskontrollsystem: https://github.github.com/training-kit/downloads/github-git-cheat-sheet/