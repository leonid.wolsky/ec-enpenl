#!/usr/bin/env python3
import sys
from py.Npp1D import *

d = Npp1D('outdata/bpm_init')

print(d.volumes)
print(d.getstate(0))

import matplotlib.pyplot as plt
plt.scatter(d.volumes[:,0], d.getstate(0)[:,0])
plt.show()

plt.scatter(d.faces[:,0], d.getflux(0)[:,0])
plt.show()

import pandas as pd

df = pd.DataFrame({'X': d.volumes[:,0], 'H+': d.getstate(0)[:,0]})
df.to_excel('./plots/concentrations.xlsx')


# plt.scatter(d.timeEvolution['time'], d.timeEvolution['voltage'])
# plt.show()