// mesh element size
lc = 1; // 1e-9m ~ Debye length
thickness = 100.0; // boundary layer thickness [1mu]
memb_thickness = 100.0; // membrane thickness [1mu]
Point(1) = {0.0, 0.0, 0.0, lc};
Point(2) = {thickness, 0.0, 0.0, lc};
Point(3) = {thickness+memb_thickness, 0.0, 0.0, lc};
Point(4) = {thickness+memb_thickness+thickness, 0.0, 0.0, lc};
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
